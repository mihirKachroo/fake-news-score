# COVID-19 fake news detector
(also available as Google Doc [here](https://docs.google.com/document/d/1ESUnwq6xj7EoO0BsT83tdrPbsx2y575ZeC19DumRPYQ/edit#heading=h.hkjmuh8jnb10))
<!-- MarkdownTOC autolink="true" -->

- [Motivation](#motivation)
- [Goals](#goals)
- [Context](#context)
- [Expected results](#expected-results)
- [Initial design directions](#initial-design-directions)
	- [User interface](#user-interface)
	- [Output data structures and formats](#output-data-structures-and-formats)
	- [Data sources](#data-sources)
	- [Aggregation algorithms](#aggregation-algorithms)
- [Organization](#organization)
	- [Schedule and core team](#schedule-and-core-team)
	- [Call for participation and skills](#call-for-participation-and-skills)
	- [How to join](#how-to-join)

<!-- /MarkdownTOC -->


## Motivation

Fake news is a type of misinformation which traditionally was being spread by yellow journalism. The rise of digital and social media platforms and, especially, their content distribution practices, have turned fake news into socially harmful and destabilizing phenomenon on a global scale. COVID-19 pandemic amplifies this trend and potentially turns the fake news into a virus itself, infecting our tensed and vulnerable minds. The vaccine and antidote against fake news is reliable and well presented information, which, in COVID-19 situation can save people's lives (e.g. [in this situation](https://www.nbcnews.com/health/health-news/man-dies-after-ingesting-chloroquine-attempt-prevent-coronavirus-n1167166)). Taking into account the urgency and precariousness of the current situation, we should not, however, forget the long term impacts of the actions that we are taking (and software we design) now and therefore importance of privacy, security and trust-less design.

This project will allow to use decentralized AI and collective action for detecting COVID-19 related fake news. It is designed to integrate into [Xccelerando browser-extension](https://gitlab.com/xccelerando/browser-extension) and will be applicable for dealing with any fake news. Below is the detailed description and call to action.

## Goals

1. To allow [Xccelerando browser-extension](https://gitlab.com/xccelerando/browser-extension) users to identify, highlight and share data about fake news in web content;
2. To present to Xccelerando browser-extension  user a transparently aggregated information about news items in the form of each item’s ‘fake-news-score’ and a convenient interface for fake news debunking / detection information used for calculating the score. An alternative way to think about this is that we want to calculate the probability that a news item contains fake news.

## Context

Xccelerando is developing a browser extension and a decentralized backend for delivering granular and precise content to users, based on their explicitly and implicitly expressed preferences, where preference database is fully owned and controlled by every user, as opposed to . We are using [Decentralized AI Alliance](https://daia.foundation/) members’ technologies, including, but not limited to those of [SingularityNET](https://singularitynet.io/), [Aigents](https://medium.com/@aigents) and [NuNet](https://medium.com/nunet/nunet-platform-demo-bf7f51dd9b1a). The extension is modular and currently consists of the following modules:

* [Brave browser extension](https://gitlab.com/xccelerando/browser-extension/brave-browser-extension);
* [RestAPI backend module](https://gitlab.com/xccelerando/browser-extension/restapi-backend);
* [Data storage module](https://gitlab.com/xccelerando/browser-extension/data-storage);
* Integration with [Aigents](https://github.com/aigents/aigents-java/issues/11) intelligent web agents network;
* [Fake news detection](https://gitlab.com/xccelerando/browser-extension/fake-news-score) and score calculation module;

Design of the decentralized backend is such that it allows to integrate new or update old modules at any time, during or after the development period. We encourage Covidathon participants to propose ideas and design modules for the integration into the decentralized backend.

## Expected results

Expected results can be separated into two components:
1. Integration of [Xccelerando browser extension](https://gitlab.com/xccelerando/browser-extension) UI and backend with the [hypothes.is](https://web.hypothes.is/) -- a conversation layer to the web;
1. A fake-news-score backend component which will integrate into the decentralized architecture of the extension backend. Note however, that the fake-news-score module will aggregate information from different data sources, which themselves will be designed as modules of this component.

## Initial design directions

Here we provide preliminary design directions. The details of the design will be worked out during initial stages of development and documented in [brave-browser-extension](https://gitlab.com/xccelerando/browser-extension/brave-browser-extension) repo (for the first component) and [fake-news-score](https://gitlab.com/xccelerando/browser-extension/fake-news-score) repo (for the second component).

<img src="schemas/full_architecture_mod.png" width="800"> 


### User interface

1. A user will be able to highlight comment and view highlighted annotations and comments via the integration with hypothes.is plugin (see [issue](xccelerando/browser-extension/brave-browser-extension#10)):

<img src="images/hypothesis_example.png" width="800"> 

The goal here is to utilize the UI and functionality of hypothes.is plugin, but pipe the data from the plugin into the xccelerando-browser-extension backend (with the help of hypothesis public API) for processing and feedback.

2. The data from fake-news-score module will be displayed near each news item in the browser extension window as component 2.3.3 as seen in the picture below (see also [related issue](https://gitlab.com/xccelerando/browser-extension/brave-browser-extension/-/issues/5)):

<img src="images/fake_news_score.png" width="800"> 

### Output data structures and formats

The data structure and format provided by the module will be determined during later stages of development, but most probably it will be a JSON structure, packing all extracted information available about particular and similar news items from data sources (possibly calculated using text similarity measures);

Our goal is to transparently aggregate information from existing credible sources, without minimal processing of this information on our side. Ideally, the functionality of the extension will not relate to ‘fake/no fake’ decision at all. The fake-news-score will be calculated from this information and used to rank (or hide) news items in browser extension interface. The goal of the fake-news-score 

### Data sources

In principle there are four types of data sources that can be used to calculate fake-news-score:
1. Information from fact checking / news debunking sites ([issue](https://gitlab.com/xccelerando/browser-extension/fake-news-score/-/issues/1));
1. Information from fake news detection algorithms ([issue(https://gitlab.com/xccelerando/browser-extension/fake-news-score/-/issues/4)]);
1. Explicit ranking, highlights and comments by Xccelerando users. On the UI side this will be done via the integration of hypothes.is plugin [issue](xccelerando/browser-extension/brave-browser-extension#10), on the backend side -- getting logged data from user behaviours database directly, most probably ([issue](https://gitlab.com/xccelerando/browser-extension/data-storage/-/issues/2));
1. Reputation of data sources. This is a kind of meta-data source, since we would calculate reputation of each other data source and that in some sort of weighting scheme when calculating fake-news-score. The obvious candidate for integration is SingularityNET reputation system (see [repo](https://github.com/singnet/reputation)).

### Aggregation algorithms

The actual fake-news-score module will aggregate data from all sources in order to calculate the probability that a particular news item contains fake news. The UI module, developed on the main xccelerando browser-plugin repo, will display this information.

It is not yet clear how many data sources we will be able to integrate and how many data sources are needed in order to obtain high quality fake news score. This can be estimated only during the development and testing. We can be fairly sure however, that we will be able to integrate data from hypotes.is plugin, which is fairly straightforward -- but not simple. The aggregation of third party information may need some additional research. We will start this aspect of the plugin with the integration of the information from news debunking sites, where actual human generated comments / mini reports are added to each debunked item / fake news.

All modules will be integrated via SingularityNET infrastructure, therefore will be decentralized and autonomous. This will allow us to integrate additional modules if and when needed.

## Organization

### Schedule and core team

Detailed project schedule, tasks and assignees will be managed via project repo https://gitlab.com/xccelerando/browser-extension.

**Core team**:
* Kabir (lead) [https://devpost.com/vveitas](https://devpost.com/vveitas), kabir@nunet.io, GitLab: @kabir.kbr;
* Eyob [https://devpost.com/eyob](https://devpost.com/eyob), 
* Dagim: [https://devpost.com/dagims](https://devpost.com/dagims), 

### Call for participation and skills

We are looking for individuals willing to contribute to the project. The implementation of proposed architecture as well as individual moduoles requires substantial resources therefore community developers are instrumental to turn it into reality. The core development team will be guiding the development and ensure the integration of the module into the architecture, but the richness of the functionality will depend crucially on community interest and involvement.

Invited contributions:
* Development of proposed modules and their components, including UI;
* Proposing and developing new modules for integration to the backend;
* Research of fake news debunking resources and establishing partnerships with them. We search for partnership with the news debunking organizations or websites; It would be ideal for the backend to find a news debunking system which has an API access. Otherwise we will have to use existing or develop a new webpage scraping tools.
* Develop an API for describing news items and fake news statements, possibly using existing API's (research needed);
* Using existing data exchange protocols, design and develop a scalable approach to ingesting data from many autonomous data sources possibly using [Ocean protocol](https://oceanprotocol.com/) and [data tokens](https://blog.oceanprotocol.com/data-tokens-1-data-custody-1d0d5ae66d0c).
* Others!

Skills (indicative list):
* We ar using: Javascript, Python, SingularityNET technologies, gRPC, Ocean protocol, git, gitlab;

### How to join

The bulk of collaborative activities will be carried out in our gitlab [fake-news-score](https://gitlab.com/xccelerando/browser-extension/fake-news-score) repository and other repos of Xccelerando [browser-extension](https://gitlab.com/xccelerando/browser-extension) group. You are welcome to head directly there, inspect the existing issues and ask questions via comments.

For direct contacts, please message Kabir#2172 on Discord channel ([invitation](https://discord.gg/gmddjvY)).