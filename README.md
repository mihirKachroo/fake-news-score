The backend module to browser extension which will calculate the faked news score for every news item displayed via xccelerando plugin. The module will aggregate information from 

1. Fake news debunkers (e.g. participants of international fact-checking network);
2. Chosen fake news detection algorithms;
3. Explicit ratings of Xccelerando users and reputation system;

Initial design document and functional requirements are here:
https://docs.google.com/document/d/1ESUnwq6xj7EoO0BsT83tdrPbsx2y575ZeC19DumRPYQ/edit#

